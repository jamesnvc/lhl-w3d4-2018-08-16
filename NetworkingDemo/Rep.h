//
//  Rep.h
//  NetworkingDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rep : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *riding;
@property (nonatomic,strong) NSString *email;

- (instancetype)initWithInfo:(NSDictionary*)info;

@end
