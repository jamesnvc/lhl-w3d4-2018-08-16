//
//  Rep.m
//  NetworkingDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Rep.h"

@implementation Rep

- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        _email = info[@"email"];
        _name = info[@"name"];
        _riding = info[@"district_name"];
    }
    return self;
}

@end
