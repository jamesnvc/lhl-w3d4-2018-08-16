//
//  NetworkManager.m
//  NetworkingDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "NetworkManager.h"
#import "Rep.h"

@implementation NetworkManager

+ (void)getRepresentatives:(void (^)(NSArray *))completion
{
    NSURL *url = [NSURL URLWithString:@"https://represent.opennorth.ca/representatives/ontario-legislature"];

//    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    // shortcut if we're just sending a GET request & don't actually need to configure the NSURLRequest
    NSLog(@"Creating task");
    NSURLSessionTask* task =
    [[NSURLSession sharedSession]
     dataTaskWithURL:url
     completionHandler:^(NSData* data, NSURLResponse*  response, NSError * error) {
         NSLog(@"Completed request: %@", response);

         // this is checking if there was an error making the request on the iOS networking level
         if (error != nil) {
             NSLog(@"Error making request: %@", error.localizedDescription);
             abort(); // indicate error somehow
         }

         // ..but we still need to check if the request was properly understood by the server -- there could be no NSError set, but the response from the server was like "400 - that was some garbage"
         NSInteger statusCode = [(NSHTTPURLResponse*)response statusCode];
         if (statusCode < 200 || statusCode >= 300) {
             // ^ we need to cast the response to an HTTPURLResponse, because  this method is general enough that it can work with non-HTTP urls, hence we have a more general response object (because URLs can be used for protocols other than HTTP & statusCode is an HTTP-specific thing
             // what this is here is "casting" which is *not* "converting" the object -- it's telling XCode "you think it's this type, but I know it's actually this other type"
             NSLog(@"Non-OK error code: %@", response);
             abort(); // indicate failure somehow
         }

         // now, we have the NSData with our information
         /* Just for demo purposes, look at our data
         NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"Got data: %@", string);
          */

         NSError *jsonError = nil;
         NSDictionary* info = [NSJSONSerialization
                    JSONObjectWithData:data
                    options:0
                    error:&jsonError];
         if (jsonError != nil) {
             NSLog(@"Error parsing JSON: %@", jsonError.localizedDescription);
             abort();
         }

//         NSLog(@"got data: %@", info);
         NSLog(@"Meta data: %@", info[@"meta"]);
         NSMutableArray *reps = [@[] mutableCopy];
         for (NSDictionary *rep in info[@"objects"]) {
             [reps addObject:[[Rep alloc] initWithInfo:rep]];
         }

         completion(reps);
    }];
    NSLog(@"Created task");
    [task resume];
    NSLog(@"Resumed task");
}

@end
