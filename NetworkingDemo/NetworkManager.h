//
//  NetworkManager.h
//  NetworkingDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+ (void)getRepresentatives:(void (^)(NSArray* reps))completion;

@end
